/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * Vlock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vlock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vlock.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stddef.h>
#include <volk.h>

#include "../utils.h"
#include "instance.h"
#include "vk_utils.h"

static const char *const extension_names[] = {
	VK_KHR_SURFACE_EXTENSION_NAME,

#ifdef VK_USE_PLATFORM_WIN32_KHR
	VK_KHR_WIN32_SURFACE_EXTENSION_NAME,
#endif
};
static const size_t extension_count = ARR_LEN(extension_names);

bool
vulk_create_inst(VkInstance *inst)
{
	VkResult res = volkInitialize();
	if (res != VK_SUCCESS) {
		perr(
				"Failed to initalize Vulkan. "
				"Do you have the Vulkan loader installed?");
		return false;
	}

	VkApplicationInfo app_info = {
		.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
		.pApplicationName = "Vlock",
		.applicationVersion = VK_MAKE_VERSION(0, 0, 0),
		.pEngineName = "Vlock Engine",
		.engineVersion = VK_MAKE_VERSION(0, 0, 0),
		.apiVersion = VK_MAKE_API_VERSION(0, 1, 3, 0),
	};

	VkInstanceCreateInfo inst_info = {
		.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
		.pApplicationInfo = &app_info,
		.enabledExtensionCount = extension_count,
		.ppEnabledExtensionNames = extension_names,
	};

	res = vkCreateInstance(&inst_info, NULL, inst);
	if (res != VK_SUCCESS) {
		perr("Failed to create Vulkan instance (%s)", vulk_strres(res));
		return false;
	}

	volkLoadInstanceOnly(*inst);

	return true;
}
