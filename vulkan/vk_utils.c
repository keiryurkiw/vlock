/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * Vlock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vlock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vlock.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#define VOLK_IMPLEMENTATION
#include <volk.h>
#include <vulkan/vk_enum_string_helper.h>

#include "vk_utils.h"

const char *
vulk_strres(VkResult res)
{
	return string_VkResult(res);
}

#ifndef NDEBUG
void
_vulk_assert(VkResult res, char *cond, char *file, int line)
{
	if (res == VK_SUCCESS)
		return;

	fprintf(stderr,
			"vlock: \x1b[01;31mvulkan assertion\x1b[0m '%s == VK_SUCCESS' "
			"failed with VkResult %d (%s) in %s:%d\n",
			cond, res, vulk_strres(res), file, line);
	abort();
}
#endif /* NDEBUG */
