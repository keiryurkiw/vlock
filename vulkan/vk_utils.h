/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * Vlock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vlock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vlock.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VULKAN_UTILS_H
#define VULKAN_UTILS_H

#include <volk.h>

const char *vulk_strres(VkResult res);

#ifndef NDEBUG
void _vulk_assert(VkResult res, char *cond, char *file, int line);
#define VULK_ASSERT(_res) _vulk_assert(_res, #_res, __FILE__, __LINE__)
#else
#define VULK_ASSERT(_res) ((void)0)
#endif /* NDEBUG */

#endif /* VULKAN_UTILS_H */
