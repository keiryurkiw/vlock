/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * Vlock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vlock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vlock.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VULKAN_INSTANCE_H
#define VULKAN_INSTANCE_H

#include <stdbool.h>
#include <volk.h>

bool vulk_create_inst(VkInstance *inst);

#endif /* VULKAN_INSTANCE_H */
