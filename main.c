/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * Vlock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vlock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vlock.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdio.h>
#include <volk.h>

#include "vulkan/instance.h"

static VkInstance vk_instance = VK_NULL_HANDLE;

static bool
init(void)
{
	if (!vulk_create_inst(&vk_instance))
		return false;

	return true;
}

static void
deinit(void)
{
	vkDestroyInstance(vk_instance, NULL);
}

int
main(void)
{
	if (!init())
		return 1;
	deinit();
}
